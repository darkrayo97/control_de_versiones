# SISTEMAS DE CONTROL DE VERSIONES
![introduccion](coso/introduccion.png)
## ***1.- Sistemas de control de versiones: Git***
## ***2.-Lenguaje de marcas: Markdown***
## ***3.-Repositorio: GitHUb***
## ***4.-Conceptos básicos Git/GitHUb***
## Resumen
* Git: es un software de control de versiones distribuido para colaborar y trabajar con otras personas en la creación de un proyecto.
* Markdown: es un lenguaje de marcas que facilita la aplicación de formato a un texto para
publicarlo en una página web.
* GitHub: es una plataforma web para alojar proyectos que usa Git como herramienta de
control de versiones. Almacena tanto documentación como software.
## Autores
Daniel Ruiz Barrenas

Moisés Moreno Moreno